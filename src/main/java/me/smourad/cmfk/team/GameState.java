package me.smourad.cmfk.team;

public enum GameState {

    WAITING,
    IN_PROGRESS,
    END

}
